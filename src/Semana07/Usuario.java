package Semana07;

public class Usuario {
    private String universidad;
    private int ciclo;
    private String curso;
    private int nivel;

    public Usuario(String universidad,int ciclo, String curso, int nivel){
        this.universidad=universidad;
        this.ciclo=ciclo;
        this.curso=curso;
        this.nivel=nivel;
    }

    public int getNivel() {
        return nivel;
    }

    public void Buscar(String titulo){
        if(Libros.getTitulo().equals(titulo)){
            System.out.println("Libros: ");
            System.out.println(Libros.getTitulo());
        }
        if(Peliculas.getTitulo().equals(titulo)){
            System.out.println("Peliculas: ");
            System.out.println(Peliculas.getTitulo());
        }
        if(Documentales.getTitulo().equals(titulo)){
            System.out.println("Documentales: ");
            System.out.println(Documentales.getTitulo());
        }
    }

}

