package Semana07;

import java.util.Objects;

public abstract class MaterialUniversitario {
    private String curso;
    private String universidad;
    private int ciclo;
    private double precio;
    private String codigo;
    public MaterialUniversitario(String curso, String codigo,String universidad, int ciclo, double precio){
        this.curso=curso;
        this.codigo= codigo;
        this.universidad=universidad;
        this.ciclo=ciclo;
        this.precio=precio;
    }
    public String getCodigo(){
        return codigo;
    }
    public MaterialUniversitario() {

    }

    public void mostrarDatos(){
        System.out.println("Curso: "+curso);
        System.out.println("Codigo: "+codigo);
        System.out.println("Universidad: "+universidad);
        System.out.println("Ciclo: "+ciclo);
        System.out.println("Precio: "+precio);
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    public String getUniversidad() {
        return universidad;
    }

    public void setUniversidad(String universidad) {
        this.universidad = universidad;
    }

    public int getCiclo() {
        return ciclo;
    }

    public void setCiclo(int ciclo) {
        this.ciclo = ciclo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MaterialUniversitario that = (MaterialUniversitario) o;
        return codigo.equals(that.codigo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }
}