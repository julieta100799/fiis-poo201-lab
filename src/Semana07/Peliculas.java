package Semana07;

public class Peliculas extends MaterialUniversitario implements Estudiable {
    private  static String titulo;
    private String genero;
    private int duracion;
    private String descripcion;

    public Peliculas(String curso, String codigo, String universidad, int ciclo, double precio, String titulo, String genero, int duracion, String descripcion){
        super(curso, codigo,universidad, ciclo, precio);
        this.titulo=titulo;
        this.genero=genero;
        this.duracion=duracion;
        this.descripcion=descripcion;
    }

    public static void setTitulo(String titulo) {
        Peliculas.titulo = titulo;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Peliculas(){
    }

    public static String getTitulo() {
        return titulo;
    }
    public void mostrarDatos(){
        System.out.println("Pelicula");
        System.out.println("Titulo: "+titulo);
        System.out.println("Genero: "+genero);
        System.out.println("Duracion: "+duracion);
        System.out.println("Descripcion: "+descripcion);
    }


    @Override
    public int obtenerTiempoEstudio() {

        return duracion;
    }
}