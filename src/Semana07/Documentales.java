package Semana07;

public class Documentales extends MaterialUniversitario implements Estudiable {
    private static String titulo;
    private String genero;
    private int duracion;
    private String descripcion;
    private int capitulo;


    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCapitulo() {
        return capitulo;
    }

    public void setCapitulo(int capitulo) {
        this.capitulo = capitulo;
    }

    public static void setTitulo(String titulo) {
        Documentales.titulo = titulo;
    }

    public Documentales(String curso, String codigo, String universidad, int ciclo, double precio, String titulo, String genero, int duracion, String descripcion, int capitulo){
        super(curso,codigo, universidad, ciclo, precio);
        this.titulo=titulo;
        this.genero=genero;
        this.duracion=duracion;
        this.descripcion=descripcion;
        this.capitulo=capitulo;

    }
    public Documentales(){

    }
    public static String getTitulo() {
        return titulo;
    }
    public void mostrarDatos(){
        System.out.println("Documental");
        System.out.println("Titulo: "+titulo);
        System.out.println("Capitulo: "+capitulo);
        System.out.println("Genero: "+genero);
        System.out.println("Duracion: "+duracion);
        System.out.println("Descripcion: "+descripcion);
    }

    @Override
    public int obtenerTiempoEstudio() {
        return duracion;
    }
}