package Semana07;

public class Libros extends MaterialUniversitario implements Estudiable {

    private static String titulo;
    private String autor;
    private int num_pag;
    private int anio;
    private String descripcion;

    public static void setTitulo(String titulo) {
        Libros.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getNum_pag() {
        return num_pag;
    }

    public void setNum_pag(int num_pag) {
        this.num_pag = num_pag;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Libros(String curso, String codigo, String universidad, int ciclo, double precio, String titulo, String autor, int num_pag, int anio, String descripcion){
        super(curso, codigo,universidad, ciclo, precio);

        this.titulo=titulo;
        this.autor=autor;
        this.num_pag=num_pag;
        this.anio=anio;
    }

    public static String getTitulo() {
        return titulo;
    }


    public void mostrarDatos(){
        System.out.println("Libro");
        System.out.println("Titulo: "+titulo);
        System.out.println("Autor: "+autor);
        System.out.println("Año de publicacion: "+anio);
        System.out.println("Numero de paginas: "+num_pag);
    }

    @Override
    public int obtenerTiempoEstudio() {
        int tiempo=num_pag*5;
        return tiempo;
    }


}
