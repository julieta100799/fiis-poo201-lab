package Semana07;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Scanner;

public class Menu {

    public static void main(String[] args) {
        Libros libros1 = new Libros("Fisica 1", "FB-402", "UNI", 3, 30, "Fisica para todos", "Alvarez", 200, 2001, "Fisica general");
        Libros libros2 = new Libros("Fisica 1", "FC-301, ", "UTEC", 3, 50, "Fisica 1", "Morales", 300, 2017, "Física para ingeniería ");
        Peliculas peliculas1 = new Peliculas("Calculo 2", "G1-302", "UNSA", 2, 10, "Calculo 2", "ciencia", 90, " Solucionario");
        Peliculas peliculas2 = new Peliculas("Calculo 2", "HU-501", "UNI", 2, 10, "Calculo integral", "ciencia", 60, "Ejercicios propuestos");
        Documentales documentales1 = new Documentales("Quimica", "FB-444", "UPN", 1, 10, "Quimica esencial", "documental", 80, " Quimica general", 1);
        Documentales documentales2 = new Documentales("Quimica", "XS-303", "UNT", 1, 15, "Quimica basica", "documental", 90, "Solucionario Completo ", 1);
        Usuario usuario1 = new Usuario("UNI", 3, "Fisica 1", 1);
        usuario1.Buscar("Fisica 1");
        Collection<Libros> libro = new ArrayList<>();
        Collection<Peliculas> peli = new ArrayList<>();
        Collection<Documentales> doc = new ArrayList<>();
        libro.add(libros1);
        libro.add(libros2);
        peli.add(peliculas1);
        peli.add(peliculas2);
        doc.add(documentales1);
        doc.add(documentales2);

        Iterator<Libros> itl=libro.iterator();
        Iterator<Peliculas> itp=peli.iterator();
        Iterator<Documentales> itd=doc.iterator();

        System.out.println("MENÚ: ");
        System.out.println("a) Agregar prodcuto universitario");
        System.out.println("b) Buscar producto universitario");
        System.out.println("c) Actualizar producto universitario");
        System.out.println("d) Eliminar producto universitario");
        Scanner sc = new Scanner(System.in);
        String resp = sc.nextLine();
        switch (resp) {
            case "a":
                System.out.println("Escoja su tipo de prducto:");
                System.out.println("-Libro");
                System.out.println("-Pelicula");
                System.out.println("-Documental");
                Scanner sc1 = new Scanner(System.in);
                String resp1 = sc1.nextLine();
                switch (resp1) {
                    case "libro":
                        libro.add(libros1);
                        libro.add(libros2);
                        libros1.mostrarDatos();
                        libros2.mostrarDatos();
                        break;
                    case "pelicula":
                        peli.add(peliculas1);
                        peli.add(peliculas2);
                        peliculas1.mostrarDatos();
                        peliculas2.mostrarDatos();
                        break;
                    case "documental":
                        doc.add(documentales1);
                        doc.add(documentales2);
                        documentales1.mostrarDatos();
                        documentales2.mostrarDatos();
                        break;
                }
            case "b":
                System.out.println("Escriba el codigo: ");
                Scanner sc2 = new Scanner(System.in);
                String resp2 = sc2.nextLine();
                if (libros1.getCodigo().equals(resp2) || libros2.getCodigo().equals(resp2) || peliculas1.getCodigo().equals(resp2)
                        || peliculas2.getCodigo().equals(resp2) || documentales1.getCodigo().equals(resp2) ||
                        documentales2.getCodigo().equals(resp2)) {
                    System.out.println("Si se encuentra disponible");
                } else {
                    System.out.println("No se encuentra disponible");
                }
                break;
            case "c":
                System.out.println("Desea actualizar producto universitario: libro, pelicula o documental? Escriba su respuesta");
                Scanner sc3= new Scanner(System.in);
                String resp3 = sc3.nextLine();
                switch (resp3) {
                    case "libro":
                        System.out.println("-------------------------------------------");
                        System.out.println("Digite el codigo de libro ha actualizar: ");
                        Scanner sc4= new Scanner(System.in);
                        String resp4 = sc4.nextLine();
                        for (Libros libro1:libro){
                            if (libro1.getCodigo().equals(resp4)){
                                while (itl.hasNext()){
                                    String codigoLibro= itl.next().getCodigo();
                                    if (libro1.getCodigo().equals(resp4)){
                                        itl.remove();
                                    }
                                }
                                libro1.setCodigo(resp4);
                                System.out.println("Digite nuevo curso: ");
                                Scanner sc5= new Scanner(System.in);
                                String resp5 = sc5.nextLine();
                                libro1.setCurso(resp5);
                                System.out.println("Digite nueva universidad: ");
                                Scanner sc6= new Scanner(System.in);
                                String resp6 = sc6.nextLine();
                                libro1.setUniversidad(resp6);
                                System.out.println("Digite nuevo ciclo: ");
                                Scanner sc7= new Scanner(System.in);
                                int resp7 = sc7.nextInt();
                                libro1.setCiclo(resp7);
                                System.out.println("Digite nuevo precio: ");
                                Scanner sc8= new Scanner(System.in);
                                double resp8 = sc8.nextDouble();
                                libro1.setPrecio(resp8);
                                System.out.println("Digite nuevo titulo: ");
                                Scanner sc9= new Scanner(System.in);
                                String resp9 = sc9.nextLine();
                                libro1.setTitulo(resp9);
                                System.out.println("Digite nuevo autor: ");
                                Scanner sc10= new Scanner(System.in);
                                String resp10 = sc10.nextLine();
                                libro1.setAutor(resp10);
                                System.out.println("Digite nuevo numero de pagina: ");
                                Scanner sc11= new Scanner(System.in);
                                int resp11= sc11.nextInt();
                                libro1.setNum_pag(resp11);
                                System.out.println("Digite nuevo año: ");
                                Scanner sc12= new Scanner(System.in);
                                int resp12= sc12.nextInt();
                                libro1.setAnio(resp12);
                                System.out.println("Digite nueva descripcion: ");
                                Scanner sc14= new Scanner(System.in);
                                String resp14 = sc14.nextLine();
                                libro1.setDescripcion(resp14);
                                System.out.println("Se ha actualizado: ");
                                libro1.mostrarDatos();
                                /*String resp6="si";
                                do {
                                    System.out.println("Desea modificar el nombre de: Curso, Universidad, Ciclo, Precio, Titulo, Autor, NumPagina, Año o Descripcion");
                                    Scanner sc7= new Scanner(System.in);
                                    String resp7= sc7.nextLine();
                                    if ()
                                    System.out.println("Desea seguir actualizando? (si/no)");
                                    Scanner sc5= new Scanner(System.in);
                                    String resp5 = sc5.nextLine();
                                    resp6=resp5;
                                }while (resp6.equalsIgnoreCase("si"));
                                */
                                System.exit(0);
                            }else{
                                System.out.println("El codigo digitado no existe");
                            }

                        }
                        break;
                    case "pelicula":
                        System.out.println("-------------------------------------------");
                        System.out.println("Digite el codigo de pelicula ha actualizar: ");
                        Scanner sc15= new Scanner(System.in);
                        String resp15= sc15.nextLine();
                        for (Peliculas peli1:peli){
                            if (peli1.getCodigo().equals(resp15)){
                                while (itl.hasNext()){
                                    String codigoPeli= itl.next().getCodigo();
                                    if (peli1.getCodigo().equals(resp15)){
                                        itl.remove();
                                    }
                                }
                                peli1.setCodigo(resp15);
                                System.out.println("Digite nuevo curso: ");
                                Scanner sc16= new Scanner(System.in);
                                String resp16= sc16.nextLine();
                                peli1.setCurso(resp16);
                                System.out.println("Digite nueva universidad: ");
                                Scanner sc17= new Scanner(System.in);
                                String resp17= sc17.nextLine();
                                peli1.setUniversidad(resp17);
                                System.out.println("Digite nuevo ciclo: ");
                                Scanner sc18= new Scanner(System.in);
                                int resp18= sc18.nextInt();
                                peli1.setCiclo(resp18);
                                System.out.println("Digite nuevo precio: ");
                                Scanner sc19= new Scanner(System.in);
                                double resp19= sc19.nextDouble();
                                peli1.setPrecio(resp19);
                                System.out.println("Digite nuevo titulo: ");
                                Scanner sc20= new Scanner(System.in);
                                String resp20= sc20.nextLine();
                                peli1.setTitulo(resp20);
                                System.out.println("Digite nuevo genero: ");
                                Scanner sc21= new Scanner(System.in);
                                String resp21= sc21.nextLine();
                                peli1.setGenero(resp21);
                                System.out.println("Digite nueva duracion: ");
                                Scanner sc22= new Scanner(System.in);
                                int resp22= sc22.nextInt();
                                peli1.setDuracion(resp22);
                                System.out.println("Digite nueva descripcion: ");
                                Scanner sc23= new Scanner(System.in);
                                String resp23= sc23.nextLine();
                                peli1.setDescripcion(resp23);
                                System.out.println("Se ha actualizado: ");
                                peli1.mostrarDatos();
                                System.exit(0);
                            }else{
                                System.out.println("El codigo digitado no existe");
                            }

                        }
                        break;
                    case "documental":
                        System.out.println("-------------------------------------------");
                        System.out.println("Digite el codigo del documental ha actualizar: ");
                        Scanner sc24= new Scanner(System.in);
                        String resp24= sc24.nextLine();
                        for (Documentales doc1:doc){
                            if (doc1.getCodigo().equals(resp24)){
                                while (itl.hasNext()){
                                    String codigoDoc= itl.next().getCodigo();
                                    if (doc1.getCodigo().equals(resp24)){
                                        itl.remove();
                                    }
                                }
                                doc1.setCodigo(resp24);
                                System.out.println("Digite nuevo curso: ");
                                Scanner sc25= new Scanner(System.in);
                                String resp25= sc25.nextLine();
                                doc1.setCurso(resp25);
                                System.out.println("Digite nueva universidad: ");
                                Scanner sc26= new Scanner(System.in);
                                String resp26= sc26.nextLine();
                                doc1.setUniversidad(resp26);
                                System.out.println("Digite nuevo ciclo: ");
                                Scanner sc27= new Scanner(System.in);
                                int resp27= sc27.nextInt();
                                doc1.setCiclo(resp27);
                                System.out.println("Digite nuevo precio: ");
                                Scanner sc28= new Scanner(System.in);
                                double resp28= sc28.nextDouble();
                                doc1.setPrecio(resp28);
                                System.out.println("Digite nuevo titulo: ");
                                Scanner sc29= new Scanner(System.in);
                                String resp29= sc29.nextLine();
                                doc1.setTitulo(resp29);
                                System.out.println("Digite nuevo genero: ");
                                Scanner sc30= new Scanner(System.in);
                                String resp30= sc30.nextLine();
                                doc1.setGenero(resp30);
                                System.out.println("Digite nueva duracion: ");
                                Scanner sc31= new Scanner(System.in);
                                int resp31= sc31.nextInt();
                                doc1.setDuracion(resp31);
                                System.out.println("Digite nueva cantidad de capitulos: ");
                                Scanner sc32= new Scanner(System.in);
                                int resp32= sc32.nextInt();
                                doc1.setCapitulo(resp32);
                                System.out.println("Digite nueva descripcion: ");
                                Scanner sc33= new Scanner(System.in);
                                String resp33= sc33.nextLine();
                                doc1.setDescripcion(resp33);
                                System.out.println("Se ha actualizado: ");
                                doc1.mostrarDatos();
                                System.exit(0);
                            }else{
                                System.out.println("El codigo digitado no existe");
                            }

                        }

                        break;
                }
                break;
            case "d":
                System.out.println("Que material desea eliminar:");
                Scanner elim = new Scanner(System.in);
                String respuesta = elim.nextLine();

                if (libros1.getCodigo().equals(respuesta) || libros2.getCodigo().equals(respuesta) || peliculas1.getCodigo().equals(respuesta)
                        || peliculas2.getCodigo().equals(respuesta) || documentales1.getCodigo().equals(respuesta) ||
                        documentales2.getCodigo().equals(respuesta)) {
                    libro.remove(libros1);
                }
                System.out.println(libro.size());
                break;
        }
    }
}
