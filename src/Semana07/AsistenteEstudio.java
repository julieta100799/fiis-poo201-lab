package Semana07;
import java.util.ArrayList;

public class AsistenteEstudio {
    Peliculas peli = new Peliculas();
    Documentales doc = new Documentales();
    private ArrayList<Estudiable> lista = new ArrayList<>();
    private Usuario usuario;

    public AsistenteEstudio(ArrayList<Estudiable> lista, Usuario usuario) {
        this.lista = lista;
        this.usuario = usuario;
    }

    public double obtenerTiempoEstudioTotal() {
        double tiempo;
        if (usuario.getNivel() == 1) {
            tiempo = 1.5 * (peli.obtenerTiempoEstudio() + doc.obtenerTiempoEstudio());
        }
        if (usuario.getNivel() == 3) {
            tiempo = 0.4 * (peli.obtenerTiempoEstudio() + doc.obtenerTiempoEstudio());
        }
        if (usuario.getNivel() == 2) {
            tiempo = peli.obtenerTiempoEstudio() + doc.obtenerTiempoEstudio();
        }
        return 0;
    }
}
