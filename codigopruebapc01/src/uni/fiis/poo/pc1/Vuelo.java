package uni.fiis.poo.pc1;

import java.util.Date;
import java.sql.Time;
import java.util.ArrayList;

public class vuelo {
    private int id_vuelo;
    private String fecha_hora_salida;
    private int duracion_estimada;
    private int cantidad_habilitada_pasajeros;
    private aeronave aeronave1;
    private menu menu1;

    public vuelo(int id_vuelo, String fecha_hora_salida, int duracion_estimada, aeronave aeronave1, menu menu1) {
        this.id_vuelo=id_vuelo;
        this.fecha_hora_salida=fecha_hora_salida;
        this.duracion_estimada=duracion_estimada;
        this.aeronave1=aeronave1;
        this.cantidad_habilitada_pasajeros=aeronave1.getCapacidad();
        this.menu1=menu1;
    }

    public void mostrar_asientos_disponibles(aeronave aeronave_indicada){
        ArrayList<asiento> asientos=aeronave1.getAsientos();
        for(int i=0;i<asientos.size();i++){
            System.out.printf(" Asiento %d está:",i+1);
            System.out.printf(asientos.get(i).getDisponibilidad());
        }
    }

    public int getId_vuelo(){
        return id_vuelo;
    }

    public String getFecha_hora_salida() {
        return fecha_hora_salida;
    }

    public int getDuracion_estimada() {
        return duracion_estimada;
    }

    public int getCantidad_habilitada_pasajeros() {
        return cantidad_habilitada_pasajeros;
    }

}
