package uni.fiis.poo.pc1;

import java.util.ArrayList;

public class menu {
    private ArrayList<String> nombre_platos;

    public menu(ArrayList<String> nombre_platos ) {
        this.nombre_platos=nombre_platos;
    }

    public void mostrar_advertencia_alergias(usuario usuario_consultor){
        for (int i=0;i<nombre_platos.size();i++){
            if (usuario_consultor.getAlergias()==nombre_platos.get(i)){
                System.out.println("Cuidado con este plato");
            }
        }

    }
}
