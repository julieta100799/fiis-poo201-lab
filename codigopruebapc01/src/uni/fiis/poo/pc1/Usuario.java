package uni.fiis.poo.pc1;

import java.util.ArrayList;

public class usuario {
    private String dni;
    private String nombre_completo;
    private String correo_electronico;
    private String telefono;
    private String alergia;
    private ArrayList<reserva> reservaciones;

    public usuario(String dni, String nombre_completo, String correo_electronico, String telefono, String alergia, ArrayList<reserva> reservaciones){
        this.dni=dni;
        this.nombre_completo=nombre_completo;
        this.correo_electronico=correo_electronico;
        this.telefono=telefono;
        this.alergia=alergia;
        reservaciones= new ArrayList<>();
    }

    public void reservar( Integer[] ids_vuelos, Integer[] cantidad_asientos_vuelo, ArrayList<Integer []> ids_asientos_por_vuelo){
        reserva reservacion = new reserva( ids_vuelos, cantidad_asientos_vuelo, ids_asientos_por_vuelo);
        reservaciones.add(reservacion);
    }
    public void ver_aerolineas(aerolinea aerolinea_a_ver){
        System.out.println(aerolinea_a_ver.getCorreo_electronico());
        System.out.println(aerolinea_a_ver.getDireccion());
        System.out.println(aerolinea_a_ver.getWeb_page());
    }

    public String getAlergias() {
        return alergia;
    }
}