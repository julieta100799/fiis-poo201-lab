package uni.fiis.poo.pc1;

public class asiento {
    private int id_asiento;
    private String disponibilidad;

    public asiento(int id_asiento){
       this.id_asiento=id_asiento;
       this.disponibilidad="Disponible";
    }

    public int getId_asiento() {
        return id_asiento;
    }

    public String getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(String disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

}
