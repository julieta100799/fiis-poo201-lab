package uni.fiis.poo.pc1;

import java.util.ArrayList;

public class reserva {
    public Integer [] ids_vuelos;
    public Integer [] cantidad_asientos_vuelo;
    public ArrayList <Integer []> ids_asientos_por_vuelo;

    public reserva (Integer[] ids_vuelos, Integer[] cantidad_asientos_vuelo, ArrayList<Integer []> ids_asientos_por_vuelo){
        this.ids_vuelos=ids_vuelos;
        this.cantidad_asientos_vuelo=cantidad_asientos_vuelo;
        this.ids_asientos_por_vuelo=ids_asientos_por_vuelo;
    }

    public void mostrar_informacion_vuelo(vuelo vuelo_escogido) {
        System.out.println("Tu vuelo con identificador "+vuelo_escogido.getId_vuelo());
        System.out.println(vuelo_escogido.getDuracion_estimada());
        System.out.println(vuelo_escogido.getFecha_hora_salida());

    }

    public void modificar_reserva(Integer[] cantidad_asientos_vuelo,ArrayList<Integer[]> ids_asientos_por_vuelo){
        this.cantidad_asientos_vuelo = cantidad_asientos_vuelo;
        this.ids_asientos_por_vuelo = ids_asientos_por_vuelo;

    }




}
