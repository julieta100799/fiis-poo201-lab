package uni.fiis.poo.pc1;

import java.util.ArrayList;
import java.util.Date;

public class aeronave {
    private String fabricante;
    private String modelo;
    private int capacidad;
    private String fecha_de_compra;
    private ArrayList<asiento> asientos;

    public aeronave(String fabricante, String modelo, int capacidad, String fecha_de_compra, ArrayList<asiento> asientos){
        this.fabricante=fabricante;
        this.modelo=modelo;
        this.capacidad=capacidad;
        this.fecha_de_compra=fecha_de_compra;
        this.asientos=asientos;
    }

    public String getFabricante() {
        return fabricante;
    }

    public String getModelo() {
        return modelo;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public String getFecha_de_compra() {
        return fecha_de_compra;
    }

    public ArrayList<asiento> getAsientos(){
        return asientos;
    }
}
