package edu.uni.poo.lab.s03;

import java.util.ArrayList;

public class Poligono {
    ArrayList<Double> lado= new ArrayList<>();
    public void mostrarReporte(){
        for( int i=0; i<lado.size(); i++){
            System.out.println(lado.get(i));
        }
    }
    public double obtenerPerimetro(){
        double perimetro = 0;
        for( int i=0; i<lado.size(); i++){
            perimetro = perimetro + lado.get(i);
        }
        return perimetro;
    }
}
