package edu.uni.poo.lab.s03;

public class Triangulo extends Poligono {

    public static double calcularArea(double lado1, double lado2, double lado3){
        double respuesta = 0;
        double semi = (lado1+lado2+lado3)/2;
        respuesta = Math.sqrt(semi*(semi-lado1)*(semi-lado2)*(semi-lado3));
        return respuesta;
    }
   public void mostrarReporte(){
        if(lado.size()==3){
            System.out.println("Es un triangulo");
        }
        obtenerTipoTriangulo(lado.get(0), lado.get(1), lado.get(2));
   }

    public static void obtenerTipoTriangulo(double lado1, double lado2, double lado3){
        if(lado1==lado2 && lado1==lado3 && lado2==lado3){
            System.out.println("El triangulo mencionado es equilatero");

        }else{
            if(lado1==lado2 || lado1==lado3 || lado2==lado3){
                System.out.println( "El triangulo es isosceles");
            }else{
                System.out.println("El triangulo es escaleno");
            }
        }
    }
}
