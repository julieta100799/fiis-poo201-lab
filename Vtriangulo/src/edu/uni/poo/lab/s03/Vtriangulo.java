package edu.uni.poo.lab.s03;

public class Vtriangulo {
    public static double calcularPerimetro(double lado1, double lado2, double lado3){
        double perimetro = 0;
        perimetro = (lado1 + lado2 + lado3);
        return perimetro;
    }

    public static double calcularArea(double lado1, double lado2, double lado3){
        double respuesta = 0;
        double semi = (lado1+lado2+lado3)/2;
        respuesta = Math.sqrt(semi*(semi-lado1)*(semi-lado2)*(semi-lado3));
        return respuesta;
    }

    public static void obtenerTipoTriangulo(double lado1, double lado2, double lado3){
        if(lado1==lado2 && lado1==lado3 && lado2==lado3){
            System.out.println("El triangulo mencionado es equilatero");

        }else{
            if(lado1==lado2 || lado1==lado3 || lado2==lado3){
                System.out.println( "El triangulo es isosceles");
            }else{
                System.out.println("El triangulo es escaleno");
            }
        }
    }

}